#!/bin/bash

#Quick parameters links
#version="5b2bc423e5842b0011ac871a"
#id="default"
#processor="xelatex"

export HOME="/usr/local/apache2/cgi-bin"


echo "Content-type: text/html"
echo ""

echo "<html>
<head>
<title>Stylo import</title>

<!--style et lien vers CSS-->
<style type=\"text/css\">code{white-space: pre;}</style>
<!--Generated CSS from stylo-->
<style type=\"text/css\">
html, body {
background: #f8f8f8;
font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;
margin: 0;
padding: 0;
width: 100%;
min-height: 100%;
overflow-x: hidden;
position: relative;
      }
      body {
      overflow-y: scroll;
      }

      #app {
      min-height: 100vh;
      }

      #app > header, .gridCenter > header {
      padding: 0.5rem 1rem;
      background-color: #7c7c7c;
      color: white;
      display: flex;
      align-items: center;
      }
      #app > header .wrapper, .gridCenter > header .wrapper {
      flex: 1 1 auto;
      }
      #app > header h1, .gridCenter > header h1 {
      margin: 0;
      flex: 0 0 auto;
      }
      #app > header nav, .gridCenter > header nav {
      flex: 0 0 auto;
      }
      #app > header nav a, .gridCenter > header nav a {
      color: white;
      margin-left: 1rem;
      }

      div#app header {
      margin-bottom: 20px;
      }
      div#app header img {
      width: 100%;
      padding-bottom: 20px;
      }
      div#app main#mainView {
      margin: 0 auto;
      width: 100%;
      max-width: 1000px;
      background-color: white;
      padding: 1em;
      }
      div#app main#mainView section h1 {
      font-size: 2em;
      margin: 0 0 1em 0;
      }

      div#app main#mainView p {
      text-align: justify;
      font-family: sans;
      font-size: 1em;
      line-height: 1.5em;
      }

      div#app main#mainView h1, div#app main#mainView h2, div#app main#mainView h3 {
      color:purple;
      }

      div#app main#mainView a{
      color:purple;
      }

      div#app main#mainView li {
      text-align: justify;
      font-family: sans;
      font-size: 1em;
      line-height: 1.4em;
      }

      div#app main#mainView blockquote {
      border-left: 4px solid lightgrey;
      padding-left: 1em;
      font-size: 1em;
      }

      div#app main#mainView sup {
      font-size: 0.7em;
      }

      div#app main#mainView .citation {
      color: darkslateblue;
      }

      div#app main#mainView .footnoteRef {
      font-weight: 600;
      }

      div#app main#mainView .references p{
      font-size: 0.9em;
      line-height: 1.3em;
      }

      div#app main#mainView .footnotes p {
      font-size: 0.9em;
      line-height: 1.3em;
      }


}
</style>
</head>

<!--corps du document-->
<body>
<div id=\"app\">
<header><h1>Stylo import</h1><div class=\"wrapper\"></div><nav><a href=\"javascript:window.close()\">close</a></nav></header>
<main id=\"mainView\"><h2>Docx to markdown</h2> "

saveIFS=$IFS
IFS='=&'
parm=($QUERY_STRING)
IFS=$saveIFS
declare -A GET
for ((i=0; i<${#parm[@]}; i+=2))
do
	GET[${parm[i]}]=${parm[i+1]}
done

#Assign defaults
endpoint="Version"

if [ "${GET[toc]}" = "true" ]; then
	toc="--table-of-contents"
else
	toc=""
fi
echo "<p>You are importing: <br>Article url: <b>${GET[url]}</b><br>Article name<b>${GET[id]}</b><br></p>"
GET[url]=${GET[url]//"%5C"/\\}
GET[url]=${GET[url]//"%3F"/?}
GET[url]=${GET[url]//"%3D"/=}
GET[url]=${GET[url]//"%26"/&}
GET[url]=${GET[url]//"%2F"/\/}
echo "${GET[url]}"

#exiting if either id or version not specified
if [ -z "${GET[id]}" ]; then
	echo "No id specified";
	echo "</body></html>"
	exit 1
fi

if [ -z "${GET[url]}" ]; then
	echo "No version specified";
	echo "</body></html>"
	exit 1
fi

#echo ${GET[source]}
#Relocate script + create folder for that version
cd "$(dirname "$0")"
mkdir ${GET[id]}
cd ${GET[id]}
wget ${GET[url]} -O ${GET[id]}.docx
#echo "curl -o ${GET[id]}.zip ${GET[source]}zip${endpoint}/${GET[version]}"
pandoc -s -f docx -t markdown --atx-header --extract-media=./ ${GET[id]}.docx -o ${GET[id]}.md


		echo "<pre>Getting ${GET[format]} file ready"
		echo "</pre>"

		#Zip all files and move ZIP and PDF to export
		echo "<pre>Getting ZIP file ready"
		zip -r ${GET[id]}.zip .
		echo "</pre>"
		mv ${GET[id]}.zip /usr/local/apache2/htdocs/export/
		mv ${GET[id]}.${GET[format]} /usr/local/apache2/htdocs/export/
		#Clean folder
		cd ..
		rm -R ${GET[version]}

		echo "<br>"

		echo "ZIP : <a href='/export/${GET[id]}.zip' target='_blank'>/export/${GET[id]}.zip</a><br>"
#case "${GET[format]}" in
#	"pdf")
#		pandoc --standalone --verbose --filter pandoc-citeproc $toc -f markdown -t latex --template=../templates/template.latex --csl ../templates/${GET[bibstyle]}.csl --pdf-engine=xelatex ${GET[id]}.md ${GET[id]}.yaml -o ${GET[id]}.pdf >> bash.log
#
#		echo "pandoc --standalone --verbose --filter pandoc-citeproc --table-of-contents -f markdown -t latex --csl ../templates/${GET[bibstyle]}.csl --pdf-engine=xelatex ${GET[id]}.md ${GET[id]}.yaml -o ${GET[id]}.pdf >> bash.log"
#
#
#
#		echo "<pre>Getting PDF file ready"
#		echo "</pre>"
#
#		#Zip all files and move ZIP and PDF to export
#		echo "<pre>Getting ZIP file ready"
#		zip -r ${GET[id]}.zip .
#		echo "</pre>"
#		mv ${GET[id]}.zip /usr/local/apache2/htdocs/export/
#		mv ${GET[id]}.pdf /usr/local/apache2/htdocs/export/
#		#Clean folder
#		cd ..
#		rm -R ${GET[version]}
#
#		echo "<br>"
#		echo "PDF : <a href='/export/${GET[id]}.pdf' target='_blank'>/export/${GET[id]}.pdf</a><br>"
#
#		echo "ZIP : <a href='/export/${GET[id]}.zip' target='_blank'>/export/${GET[id]}.zip</a><br>"
#
#		;;
#	"tex")
#		pandoc --standalone --verbose --filter pandoc-citeproc $toc -f markdown -t latex --template=../templates/template.latex --csl ../templates/${GET[bibstyle]}.csl ${GET[id]}.md ${GET[id]}.yaml -o ${GET[id]}.tex >> bash.log
#echo "<br>"
#echo "pandoc --standalone --verbose --filter pandoc-citeproc $toc -f markdown -t latex --csl ../templates/${GET[bibstyle]}.csl ${GET[id]}.md ${GET[id]}.yaml -o ${GET[id]}.tex >> bash.log"
#		
## xelatex --interaction=batchmode ${GET[id]}.tex >> bash.log
##	xelatex --interaction=batchmode ${GET[id]}.tex
#
#
#		echo "<pre>Getting tex file ready"
#		echo "</pre>"
#
#		#Zip all files and move ZIP and PDF to export
#		echo "<pre>Getting ZIP file ready"
#		zip -r ${GET[id]}.zip .
#		echo "</pre>"
#		mv ${GET[id]}.zip /usr/local/apache2/htdocs/export/
#		mv ${GET[id]}.tex /usr/local/apache2/htdocs/export/
#		#Clean folder
#		cd ..
#		rm -R ${GET[version]}
#
#		echo "<br>"
#		echo "tex : <a href='/export/${GET[id]}.tex' target='_blank'>/export/${GET[id]}.tex</a><br>"
#
#		echo "ZIP : <a href='/export/${GET[id]}.zip' target='_blank'>/export/${GET[id]}.zip</a><br>"
#
#		;;
#	"xml")   	
#		pandoc --standalone --verbose --filter pandoc-citeproc --section-divs --ascii --template=../templates/templateHtml5-preview.html5 $toc -f markdown -t html5 --csl ../templates/${GET[bibstyle]}.csl ${GET[id]}.md ${GET[id]}.yaml -o ${GET[id]}.html >> bash.log
#		#pandoc --standalone --verbose --filter pandoc-citeproc -f markdown -t html5 --csl ../templates/chicagomodified.csl ${GET[id]}.md ${GET[id]}.yaml -o ${GET[id]}.html >> bash.log
#
#java  -cp /usr/local/vendor/saxon9he.jar:/usr/local/vendor/tagsoup-1.2.1.jar net.sf.saxon.Transform -x:org.ccil.cowan.tagsoup.Parser -s:${GET[id]}.html -xsl:../templates/XHTML52erudit.xsl -o:${GET[id]}.xml !indent=yes
#		echo "<pre>Getting ${GET[format]} file ready"
#		echo "</pre>"
#
#		#Zip all files and move ZIP and PDF to export
#		echo "<pre>Getting ZIP file ready"
#		zip -r ${GET[id]}.zip .
#		echo "</pre>"
#		mv ${GET[id]}.zip /usr/local/apache2/htdocs/export/
#		mv ${GET[id]}.${GET[format]} /usr/local/apache2/htdocs/export/
#		#Clean folder
#		cd ..
#		rm -R ${GET[version]}
#
#		echo "<br>"
#		echo "${GET[format]} : <a href='/export/${GET[id]}.${GET[format]}' target='_blank'>/export/${GET[id]}.${GET[format]}</a><br>"
#
#		echo "ZIP : <a href='/export/${GET[id]}.zip' target='_blank'>/export/${GET[id]}.zip</a><br>"
#		;;
#	zip)   	
#
#		echo "<pre>Getting ${GET[format]} file ready"
#		echo "</pre>"
#
#		#Zip all files and move ZIP and PDF to export
#		echo "<pre>Getting ZIP file ready"
#		zip -r ${GET[id]}.zip .
#		echo "</pre>"
#		mv ${GET[id]}.zip /usr/local/apache2/htdocs/export/
#		mv ${GET[id]}.${GET[format]} /usr/local/apache2/htdocs/export/
#		#Clean folder
#		cd ..
#		rm -R ${GET[version]}
#
#		echo "<br>"
#
#		echo "ZIP : <a href='/export/${GET[id]}.zip' target='_blank'>/export/${GET[id]}.zip</a><br>"
#		;;
#	html5)   	
#		pandoc --standalone --verbose --filter pandoc-citeproc --section-divs --ascii --template=../templates/templateHtml5-preview.html5 $toc -f markdown -t ${GET[format]} --csl ../templates/${GET[bibstyle]}.csl ${GET[id]}.md ${GET[id]}.yaml -o ${GET[id]}.html >> bash.log
#		 echo " pandoc --standalone --verbose --filter pandoc-citeproc --section-divs --ascii --template=../templates/templateHtml5-preview.html5 $toc -f markdown -t ${GET[format]} --csl ../templates/${GET[bibstyle]}.csl ${GET[id]}.md ${GET[id]}.yaml -o ${GET[id]}.html >> bash.log"
#
#		echo "<pre>Getting ${GET[format]} file ready"
#		echo "</pre>"
#
#		#Zip all files and move ZIP and PDF to export
#		echo "<pre>Getting ZIP file ready"
#		zip -r ${GET[id]}.zip .
#		echo "</pre>"
#		mv ${GET[id]}.zip /usr/local/apache2/htdocs/export/
#		mv ${GET[id]}.html /usr/local/apache2/htdocs/export/
#		#Clean folder
#		cd ..
#		rm -R ${GET[version]}
#
#		echo "<br>"
#		echo "${GET[format]} : <a href='/export/${GET[id]}.html' target='_blank'>/export/${GET[id]}.html</a><br>"
#echo "<b>The images files are in the zip archive</b><br>"
#		echo "ZIP : <a href='/export/${GET[id]}.zip' target='_blank'>/export/${GET[id]}.zip</a><br>"
#		;;
#	epub)   	
#		pandoc --standalone --verbose --filter pandoc-citeproc $toc -f markdown -t epub --csl ../templates/${GET[bibstyle]}.csl ${GET[id]}.md ${GET[id]}.yaml -o ${GET[id]}.epub >> bash.log
#
#	echo	"pandoc --standalone --verbose --filter pandoc-citeproc $toc -f markdown -t ${GET[format]} --csl ../templates/${GET[bibstyle]}.csl ${GET[id]}.md ${GET[id]}.yaml -o ${GET[id]}.${GET[format]} >> bash.log"
#		echo "<pre>Getting ${GET[format]} file ready"
#		echo "</pre>"
#
#		#Zip all files and move ZIP and PDF to export
#		echo "<pre>Getting ZIP file ready"
#		zip -r ${GET[id]}.zip .
#		echo "</pre>"
#		mv ${GET[id]}.zip /usr/local/apache2/htdocs/export/
#		mv ${GET[id]}.${GET[format]} /usr/local/apache2/htdocs/export/
#		#Clean folder
#		cd ..
#		rm -R ${GET[version]}
#
#		echo "<br>"
#		echo "${GET[format]} : <a href='/export/${GET[id]}.${GET[format]}' target='_blank'>/export/${GET[id]}.${GET[format]}</a><br>"
#
#		echo "ZIP : <a href='/export/${GET[id]}.zip' target='_blank'>/export/${GET[id]}.zip</a><br>"
#		;;
#	*)   	
#		pandoc --standalone --verbose --filter pandoc-citeproc $toc -f markdown -t ${GET[format]} --csl ../templates/${GET[bibstyle]}.csl ${GET[id]}.md ${GET[id]}.yaml -o ${GET[id]}.${GET[format]} >> bash.log
#
#	echo	"pandoc --standalone --verbose --filter pandoc-citeproc $toc -f markdown -t ${GET[format]} --csl ../templates/${GET[bibstyle]}.csl ${GET[id]}.md ${GET[id]}.yaml -o ${GET[id]}.${GET[format]} >> bash.log"
#		echo "<pre>Getting ${GET[format]} file ready"
#		echo "</pre>"
#
#		#Zip all files and move ZIP and PDF to export
#		echo "<pre>Getting ZIP file ready"
#		zip -r ${GET[id]}.zip .
#		echo "</pre>"
#		mv ${GET[id]}.zip /usr/local/apache2/htdocs/export/
#		mv ${GET[id]}.${GET[format]} /usr/local/apache2/htdocs/export/
#		#Clean folder
#		cd ..
#		rm -R ${GET[version]}
#
#		echo "<br>"
#		echo "${GET[format]} : <a href='/export/${GET[id]}.${GET[format]}' target='_blank'>/export/${GET[id]}.${GET[format]}</a><br>"
#
#		echo "ZIP : <a href='/export/${GET[id]}.zip' target='_blank'>/export/${GET[id]}.zip</a><br>"
#		;;
#esac
#
#
#
#echo "
#<h3>More export options</h3>
#<form action=\"/cgi-bin/exportArticle/exec.cgi\" method=\"get\">
#<input type=\"hidden\" name=\"source\" value=\"${GET[source]}\"> 
#<input type=\"hidden\" name=\"id\" value=\"${GET[id]}\">
#<input type=\"hidden\" name=\"version\" value=\"${GET[version]}\">
#Format:
#<select name=\"format\">
#<option value=\"pdf\">PDF</option>
#<option value=\"tex\">tex</option>
#<option value=\"html5\">HTML</option>
#<option value=\"xml\">XML eruditArticle</option>
#<option value=\"odt\">odt</option>
#<option value=\"docx\">docx</option>
#<option value=\"tei\">TEI Lite</option>
#<option value=\"epub\">epub</option>
#
#</select>
#<br/>
#<br/>
#Bibliographical style:  
#
#<select name=\"bibstyle\">
#<option value=\"chicagomodified\">Chicago inline</option>
#<option value=\"chicago-fullnote-bibliography-fr\">Chicago footnotes</option>
#<option value=\"lettres-et-sciences-humaines-fr\">Lettres et sciences humaines</option>
#</select>
#<br/>
#Processor: <br>
#<label><input type=\"radio\" name=\"processor\" value=\"xelatex\" checked>xelatex</label><br>
#<label><input type=\"radio\" name=\"processor\" value=\"pdflatex\">pdflatex</label><br>
#
#<input type=\"submit\" value=\"Submit\">
#</form>"
#
##echo "<a href=\"/cgi-bin/exportArticle/exec.cgi?id=${GET[id]}&version=${GET[version]}&processor=${GET[procesor]}&source=${GET[source]}&format=html&bibstyle=${GET[bibstyle]}\">Export en HTML</a>"
##echo "<a href=\"/cgi-bin/exportArticle/exec.cgi?id=${GET[id]}&version=${GET[version]}&processor=${GET[procesor]}&source=${GET[source]}&format=docx&bibstyle=${GET[bibstyle]}\">Export en DOCX</a>"
##echo "<a href=\"/cgi-bin/exportArticle/exec.cgi?id=${GET[id]}&version=${GET[version]}&processor=${GET[procesor]}&source=${GET[source]}&format=tei&bibstyle=${GET[bibstyle]}\">Export en TEI Lite</a>"
##echo "<a href=\"/cgi-bin/exportArticle/exec.cgi?id=${GET[id]}&version=${GET[version]}&processor=${GET[procesor]}&source=${GET[source]}&format=odt&bibstyle=${GET[bibstyle]}\">Export en ODT</a>"
##echo "<a href=\"/cgi-bin/exportArticle/exec.cgi?id=${GET[id]}&version=${GET[version]}&processor=${GET[procesor]}&source=${GET[source]}&format=pdf&bibstyle=${GET[bibstyle]}\">Export en PDF</a>"
##echo "<a href=\"/cgi-bin/exportArticle/exec.cgi?id=${GET[id]}&version=${GET[version]}&processor=${GET[procesor]}&source=${GET[source]}&format=${GET[format]}&bibstyle=chicago-fullnote-bibliography-fr.csl\">Export en Chicago Fullnote</a>"
##echo "</main></div></body></html>"
