\documentclass[10pt,french,letterpaper]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\usepackage[french]{babel}

%\usepackage{fontspec}
%\setmainfont{Times New Roman}


\usepackage[a4paper]{geometry}
\geometry{top=2cm, bottom=2cm, left=4cm, right=4cm}
\usepackage[document]{ragged2e}
\usepackage{graphicx}
\usepackage{fancyhdr}
\usepackage{biblatex}
\usepackage{csquotes}

\usepackage{mathptmx}%choix de la police mathptmx = Times

\usepackage[singlespacing]{setspace}

\pagestyle{fancy}
\fancyhf{}
\rhead{}
\lhead{ }
\cfoot{\footnotesize{$if(authors)$
$for(authors)$$authors.surname$$sep$,  $endfor$
$endif$ -- $if(cours)$
$for(cours)$$cours.id$  $endfor$
$endif$ -- $typeTravail$ -- $session$ $year$ \\
\thepage}}

\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript

\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0.5pt}


$if(mathfont)$
$if(mathspec)$
  \ifxetex
    \setmathfont(Digits,Latin,Greek)[$for(mathfontoptions)$$mathfontoptions$$sep$,$endfor$]{$mathfont$}
  \else
    \setmathfont[$for(mathfontoptions)$$mathfontoptions$$sep$,$endfor$]{$mathfont$}
  \fi
$else$
  \setmathfont[$for(mathfontoptions)$$mathfontoptions$$sep$,$endfor$]{$mathfont$}
$endif$
$endif$

% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}

% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage[$for(microtypeoptions)$$microtypeoptions$$sep$,$endfor$]{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\PassOptionsToPackage{hyphens}{url} % url is loaded by hyperref
$if(verbatim-in-note)$
\usepackage{fancyvrb}
$endif$
\usepackage[unicode=true]{hyperref}
$if(colorlinks)$
\PassOptionsToPackage{usenames,dvipsnames}{color} % color is loaded by hyperref
$endif$
\hypersetup{
$if(title-meta)$
            pdftitle={$title-meta$$if(subtitle)$ : $subtitle$$endif$},
$endif$
$if(authors)$
            pdfauthor={$for(authors)$$authors.forname$ $authors.surname$$sep$, $endfor$},
$endif$
$if(keywords)$
            pdfkeywords={$for(keywords)$$keywords.list$$sep$, $endfor$},
$endif$
$if(colorlinks)$
            colorlinks=true,
            linkcolor=$if(linkcolor)$$linkcolor$$else$Maroon$endif$,
            citecolor=$if(citecolor)$$citecolor$$else$Blue$endif$,
            urlcolor=$if(urlcolor)$$urlcolor$$else$Blue$endif$,
$else$
            pdfborder={0 0 0},
$endif$
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
$if(verbatim-in-note)$
\VerbatimFootnotes % allows verbatim text in footnotes
$endif$
$if(geometry)$
\usepackage[$for(geometry)$$geometry$$sep$,$endfor$]{geometry}
$endif$

$if(natbib)$
\usepackage{natbib}
\bibliographystyle{$if(biblio-style)$$biblio-style$$else$plainnat$endif$}
$endif$
$if(biblatex)$
\usepackage[$if(biblio-style)$style=$biblio-style$,$endif$$for(biblatexoptions)$$biblatexoptions$$sep$,$endfor$]{biblatex}
$for(bibliography)$
\addbibresource{$bibliography$}
$endfor$
$endif$
$if(listings)$
\usepackage{listings}
$endif$
$if(lhs)$
\lstnewenvironment{code}{\lstset{language=Haskell,basicstyle=\small\ttfamily}}{}
$endif$
$if(highlighting-macros)$
$highlighting-macros$
$endif$
$if(tables)$
\usepackage{longtable,booktabs}
% Fix footnotes in tables (requires footnote package)
\IfFileExists{footnote.sty}{\usepackage{footnote}\makesavenoteenv{long table}}{}
$endif$

\usepackage{graphicx,grffile}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}

$if(links-as-notes)$
% Make links footnotes instead of hotlinks:
\renewcommand{\href}[2]{#2\footnote{\url{#1}}}
$endif$
$if(strikeout)$
\usepackage[normalem]{ulem}
% avoid problems with \sout in headers with hyperref:
\pdfstringdefDisableCommands{\renewcommand{\sout}{}}
$endif$
$if(indent)$
$else$
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
$endif$
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
$if(numbersections)$
\setcounter{secnumdepth}{$if(secnumdepth)$$secnumdepth$$else$5$endif$}
$else$
\setcounter{secnumdepth}{0}
$endif$
$if(subparagraph)$
$else$
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi
$endif$
$if(dir)$
\ifxetex
  % load bidi as late as possible as it modifies e.g. graphicx
  $if(latex-dir-rtl)$
  \usepackage[RTLdocument]{bidi}
  $else$
  \usepackage{bidi}
  $endif$
\fi
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \TeXXeTstate=1
  \newcommand{\RL}[1]{\beginR #1\endR}
  \newcommand{\LR}[1]{\beginL #1\endL}
  \newenvironment{RTL}{\beginR}{\endR}
  \newenvironment{LTR}{\beginL}{\endL}
\fi
$endif$

% set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother


$if(title_f)$
\title{$title_f$$if(thanks)$\thanks{$thanks$}$endif$}
$endif$
$if(subtitle_f)$
\providecommand{\subtitle}[1]{}
\subtitle{$subtitle_f$}
$endif$
$if(authors)$
\author{$for(authors)$$authors.forname$ $authors.surname$$sep$ \and $endfor$}
$endif$

$if(cours)$
\providecommand{\cours}[1]{}
\cours{$for(cours)$$cours.id$ $cours.title$$sep$ \and $endfor$}
$endif$
\cours{}
\date{}

$if(teachers)$
\providecommand{\teacher{$for(teachers)$$teachers.forname$ $teachers.surname$$sep$ \and $endfor$}}
$endif$


\begin{document}

$if(authors)$
$for(authors)$
\par\noindent\rlap{
    \large\textbf{$authors.forname$ $authors.surname$}
    }
\hfill\hfill\llap{
  \footnotesize{$authors.matricule$}
  }
\par
$endfor$
$endif$
\vspace*{0.5cm}

\raggedright \Large{\textbf{$typeTravail$ : $title_f$ $if(subtitle_f)$ : $subtitle_f$$endif$}}

\vspace*{0.5cm}
\footnotesize{\copyright $year$ par $for(authors)$$authors.forname$ $authors.surname$$sep$,  $endfor$. Ce travail a été réalisé à l\textquoteright EBSI, Université de Montréal, dans le cadre du cours $if(cours)$ $for(cours)$ \textbf{$cours.id$ $cours.title$}  $endfor$
$endif$ donné au trimestre d\textquoteright \lowercase{$session$} $year$ par $for(teachers)$$teachers.forname$ $teachers.surname$ $endfor$ (remis le $day$ $month$ $year$).}
\hrule
\vspace*{0.5cm}

$body$


$if(natbib)$
$if(bibliography)$
$if(biblio-title)$
$if(book-class)$
\renewcommand\bibname{$biblio-title$}
$else$
\renewcommand\refname{$biblio-title$}
$endif$
$endif$
\bibliography{$for(bibliography)$$bibliography$$sep$,$endfor$}

$endif$
$endif$
$if(biblatex)$
\printbibliography$if(biblio-title)$[title=$biblio-title$]$endif$

$endif$

\end{document}
