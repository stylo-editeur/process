#!/bin/bash

#Quick parameters links
#version="5b2bc423e5842b0011ac871a"
#id="default"
#processor="xelatex"

export HOME="/usr/local/apache2/cgi-bin"


echo "Content-type: text/html"
echo ""

echo "
<html>
<head>
<title>Stylo export</title>
<meta charset="UTF-8"/>
<!--style et lien vers CSS-->
<style type=\"text/css\">code{white-space: pre;}</style>
<!--Generated CSS from stylo-->
<style type=\"text/css\">
html, body {
background: #f8f8f8;
font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;
margin: 0;
padding: 0;
width: 100%;
min-height: 100%;
overflow-x: hidden;
position: relative;
      }
      body {
      overflow-y: scroll;
      }

      #app {
      min-height: 100vh;
      }

      #app > header, .gridCenter > header {
      padding: 0.5rem 1rem;
      background-color: #7c7c7c;
      color: white;
      display: flex;
      align-items: center;
      }
      #app > header .wrapper, .gridCenter > header .wrapper {
      flex: 1 1 auto;
      }
      #app > header h1, .gridCenter > header h1 {
      margin: 0;
      flex: 0 0 auto;
      }
      #app > header nav, .gridCenter > header nav {
      flex: 0 0 auto;
      }
      #app > header nav a, .gridCenter > header nav a {
      color: white;
      margin-left: 1rem;
      }

      div#app header {
      margin-bottom: 20px;
      }
      div#app header img {
      width: 100%;
      padding-bottom: 20px;
      }
      div#app main#mainView {
      margin: 0 auto;
      width: 100%;
      max-width: 1000px;
      background-color: white;
      padding: 1em;
      }
      div#app main#mainView section h1 {
      font-size: 2em;
      margin: 0 0 1em 0;
      }

      div#app main#mainView p {
      text-align: justify;
      font-family: sans;
      font-size: 1em;
      line-height: 1.5em;
      }

      div#app main#mainView h1, div#app main#mainView h2, div#app main#mainView h3 {
      color:purple;
      }

      div#app main#mainView a{
      color:purple;
      }

      div#app main#mainView li {
      text-align: justify;
      font-family: sans;
      font-size: 1em;
      line-height: 1.4em;
      }

      div#app main#mainView blockquote {
      border-left: 4px solid lightgrey;
      padding-left: 1em;
      font-size: 1em;
      }

      div#app main#mainView sup {
      font-size: 0.7em;
      }

      div#app main#mainView .citation {
      color: darkslateblue;
      }

      div#app main#mainView .footnoteRef {
      font-weight: 600;
      }

      div#app main#mainView .references p{
      font-size: 0.9em;
      line-height: 1.3em;
      }

      div#app main#mainView .footnotes p {
      font-size: 0.9em;
      line-height: 1.3em;
      }


}
</style>
</head>

<!--corps du document-->
<body>
<div id=\"app\">
<header><h1>Stylo Export</h1><div class=\"wrapper\"></div><nav><a href=\"javascript:window.close()\">close</a></nav></header>
<main id=\"mainView\"><h2>Export of a Stylo article for Université de Montréal</h2> "


#Assign parameters : exec.cgi?id=SP002&version=5b2be647e5842b0011ac874d&processor=pdflatex
#returns ${GET[id]} ${GET[version]} ${GET[processor]}
saveIFS=$IFS
IFS='=&'
parm=($QUERY_STRING)
IFS=$saveIFS
declare -A GET
for ((i=0; i<${#parm[@]}; i+=2))
do
    GET[${parm[i]}]=${parm[i+1]}
done

#Assign defaults
endpoint="Version"
if [ "${GET[toc]}" = "true" ]; then
	toc="--table-of-contents"
else
	toc=""
fi

echo "<p>id : ${GET[id]}<br> Article : ${GET[article]}<br> Version : ${GET[version]}<br>Processor : ${GET[processor]}</p>"

#exiting if either id or version not specified
if [ -z "${GET[id]}" ]; then
    echo "No id specified";
    echo "</body></html>"
    exit 1
fi
#if [ -n "${GET[article]}"]; then
#    GET[version]=${GET[article]}
#    echo "THis is an article"
#    endpoint="Article"
#fi
if [ -z "${GET[version]}" ]; then
    echo "No version specified";
    echo "</body></html>"
    exit 1
fi

#Relocate script + create folder for that version
cd "$(dirname "$0")"
mkdir ${GET[version]}
cd ${GET[version]}

curl -v -o ${GET[id]}.zip https://stylo.ecrituresnumeriques.ca/export/${endpoint}/${GET[version]}/zip
# curl -o ${GET[id]}.html https://stylo.ecrituresnumeriques.ca/api/v1/html${endpoint}/${GET[version]}
wget -nd -p -H -P media/ -A jpeg,jpg,bmp,gif,png -e robots=off https://stylo.ecrituresnumeriques.ca/export/${endpoint}/${GET[version]}/html
unzip ${GET[id]}.zip >> bash.log
rm ${GET[id]}.zip

rename "s/${GET[version]}/${GET[id]}/g" *
sed -i -e "s/${GET[version]}/${GET[id]}/g" ${GET[id]}.yaml

echo "tout va bien pour le moment"

# on copie le md de manière à produire un html qui conserve le lien aux images (par exemple, NV fonctionnera avec des images provenant de son WP)
cp ${GET[id]}.md ${GET[id]}.wp.md

if find media/ -mindepth 1 -print -quit 2>/dev/null | grep -q .; then
	cd media
	echo "You have media file(s):<br>"
	COUNTER=0
	for filename in "$3"*; do
		COUNTER=$[$COUNTER +1]
		echo "<b>${filename%.*}</b> will be renamed <b>${GET[id]}-img${COUNTER}.*</b><br>"
		sed -i -e "s@${filename%.*}@${GET[id]}\-img${COUNTER}@g" ../${GET[id]}.md
		mv ${filename%.*}.${filename##*.} ${GET[id]}-img${COUNTER}.${filename##*.}
		sed -i -e "s@\!\[\(.\+\)\](.*-img${COUNTER}@![\1](media/${GET[id]}-img${COUNTER}@g" ../${GET[id]}.md
	done
	cd ..
fi

# conversion tex prenant en compte la présence d'une toc ou non
pandoc --standalone --verbose --filter pandoc-citeproc $toc -f markdown -t latex --template=../templates/${GET[template]}.latex --csl ../templates/universite-de-montreal-apa.csl ${GET[id]}.yaml --bibliography=${GET[id]}.bib ${GET[id]}.md -o ${GET[id]}.tex

echo "pandoc --standalone --verbose --filter pandoc-citeproc $toc -f markdown -t latex --template=../templates/${GET[template]}.latex --csl ../templates/universite-de-montreal-apa.csl ${GET[id]}.yaml --bibliography=${GET[id]}.bib ${GET[id]}.md -o ${GET[id]}.tex"

#Copy assets in the /media files
cp -r ../assets/* ./media


#create PDF using processor
echo "<br/>Voici la liste des fichiers dans template<br/>"
ls ../templates
echo "<br/>fin de la liste<br/>"
xelatex ${GET[id]}.tex >> bash.log
echo "xelatex 1 <br>"
xelatex ${GET[id]}.tex >> bash.log
echo "xelatex 2 <br>"
xelatex ${GET[id]}.tex >> bash.log
echo "xelatex 3 <br>"

#Zip all files and move ZIP and PDF to export
echo "<pre>Getting ZIP file ready"
zip -r ${GET[id]}.zip .
echo "</pre>"
mv ${GET[id]}.zip /usr/local/apache2/htdocs/export/
mv ${GET[id]}.pdf /usr/local/apache2/htdocs/export/

#Clean folder
cd ..
rm -R ${GET[version]}

echo "<br>"
echo "PDF : <a href='/export/${GET[id]}.pdf' target='_blank'>/export/${GET[id]}.pdf</a><br>"
echo "ZIP : <a href='/export/${GET[id]}.zip' target='_blank'>/export/${GET[id]}.zip</a><br>"

echo "</body></html>"
