FROM httpd

RUN apt-get update && apt-get install -y curl wget unzip zip rename 
RUN apt-get install -y texlive texlive-lang-french texlive-latex-extra texlive-xetex 
run apt-get install -y python-pip wget texlive-bibtex-extra biber
RUN mkdir -p /usr/share/man/man1
RUN apt-get install -y default-jre
COPY ./vendor /usr/local/vendor/
RUN dpkg -i /usr/local/vendor/pandoc-2.4-1-amd64.deb
RUN tar -xzf /usr/local/vendor/linux-ghc86-pandoc24.tar.gz && \
    mv pandoc-crossref /usr/bin/ && \
    pip install pandocfilters && \
    apt-get clean -y && \
    rm -rf /usr/local/vendor/pandoc-2.4-1-amd64.deb /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN apt update && apt install -y texlive-latex-recommended texlive-lang-*
RUN apt update && apt install libxml2-utils

COPY ./html/ /usr/local/apache2/htdocs/
COPY ./config/httpd.conf /usr/local/apache2/conf/httpd.conf
COPY ./cgi-bin /usr/local/apache2/cgi-bin

# Give permission to Daemon to cgi-bin
RUN chown daemon:daemon -R /usr/local/apache2/cgi-bin
RUN chown daemon:daemon -R /usr/local/apache2/htdocs/export/
